package com.zeitwesen.assessment;

import static java.lang.String.format;

import java.util.Objects;
import java.util.Optional;


/**
 * Immutable model to represent a {@link Link}
 */
class Link {

	private final String url;


	private Link(String uri) {
		this.url = Optional.of(uri).filter(s -> !(s.trim().isEmpty())).orElseThrow(
				() -> new IllegalArgumentException(
						format("URL of %s must not be empty!", getClass().getSimpleName())));
	}


	@Override
	public int hashCode() {
		return Objects.hash(url);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		Link other = (Link) obj;
		return Objects.equals(url, other.getUrl());
	}


	@Override
	public String toString() {
		return "Link [url=" + url + "]";
	}


	String getUrl() {
		return url;
	}
	
	/**
	 * Factory method to create a {@link Link} object and to have a nice fluent-style API 
	 * @param url
	 * @return {@link Link} instance
	 */
	static Link of(String url) {
		return new Link(url);
	}

}
