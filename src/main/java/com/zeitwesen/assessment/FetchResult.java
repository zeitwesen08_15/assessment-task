package com.zeitwesen.assessment;

import static java.util.Objects.requireNonNull;

import java.util.Objects;
import java.util.Optional;


/**
 * Immutable model to encapsulate a {@link FetchResult}
 */
class FetchResult {

	private final boolean successful;
	private final String md5;
	private final String base64;
	private final Link link;


	/**
	 * Creates a {@link Link} model holding the checksum and a referenced {@link Link}
	 * 
	 * @param md5
	 * @param link
	 */
	private FetchResult(boolean successful, String md5, String base64, Link link) {
		this.successful = successful;
		this.md5 = md5;
		this.base64 = base64;
		this.link = link;
	}


	@Override
	public int hashCode() {
		return Objects.hash(successful, md5, base64, link);
	}


	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null || getClass() != obj.getClass()) {
			return false;
		}
		FetchResult other = (FetchResult) obj;
		return Objects.equals(successful, other.isSuccessful()) && Objects.equals(md5, other.getMd5())
				&& Objects.equals(base64, other.getBase64()) && Objects.equals(link, other.getLink());
	}


	@Override
	public String toString() {
		return "FetchResult [successful=" + successful + ", md5=" + md5 + ", base64=" + base64 + ", link=" + link + "]";
	}


	Link getLink() {
		return link;
	}


	String getMd5() {
		return md5;
	}


	String getBase64() {
		return base64;
	}


	boolean isSuccessful() {
		return successful;
	}


	/**
	 * Factory method to create a successfull(!) {@link FetchResult}
	 * @param md5
	 * @param base64
	 * @param link
	 * @return
	 */
	static FetchResult sucess(String md5, String base64, Link link) {
		return new FetchResult(
				true,
				requireNonNull(
						Optional.of(md5).filter(s -> !(s.trim().isEmpty()))
								.orElseThrow(
										() -> new IllegalArgumentException(
												String.format(
														"Md5 of %s must not be empty!",
														FetchResult.class.getSimpleName())))
								.toLowerCase()),
				Optional.of(base64).filter(s -> !(s.trim().isEmpty())).orElseThrow(
						() -> new IllegalArgumentException(
								String.format("Base64 of %s must not be empty!", FetchResult.class.getSimpleName()))),
						requireNonNull(link));
	}

	/**
	 * Factory method to create failed(!) {@link FetchResult}
	 * @param link
	 * @return
	 */
	static FetchResult fail(Link link) {
		return new FetchResult(false, null, null, requireNonNull(link));
	}

}
