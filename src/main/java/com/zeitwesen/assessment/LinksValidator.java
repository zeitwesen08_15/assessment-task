package com.zeitwesen.assessment;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.function.Predicate;

import javax.inject.Singleton;

import org.slf4j.Logger;


@Singleton
class LinksValidator {

	private static final Logger LOG = getLogger(LinksValidator.class);

	private static final Predicate<Link> CUSTOM_URI_VALIDATOR = new CustomUriValidator();


	List<Link> validate(List<Link> links) {
		return requireNonNull(links).parallelStream().filter(CUSTOM_URI_VALIDATOR).collect(toImmutableList());
	}

	private static class CustomUriValidator implements Predicate<Link> {

		@Override
		public boolean test(Link link) {

			try {
				URI uri = new URI(link.getUrl());
				return uri.getPort() == -1 && uri.getUserInfo() == null && uri.getQuery() == null
						&& uri.getFragment() == null;
			} catch (URISyntaxException e) {
				// in this case it can never every be a valid URI
				LOG.error("Error validating {}", link.getUrl(), e);
				return false;
			}

		}

	}

}
