package com.zeitwesen.assessment;

import static com.google.common.base.Functions.identity;
import static java.lang.System.lineSeparator;
import static java.nio.file.Files.exists;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static java.util.stream.Collectors.toMap;

import java.io.PrintStream;
import java.nio.file.Path;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.inject.Inject;
import javax.inject.Singleton;


@Singleton
class AssessmentProcessor {

	private final FileReader fileReader;
	private final LinkParser linkParser;
	private final LinksValidator linksValidator;
	private final ParallelLinkFetcher parallelLinkFetcher;
	private final PrintStream outStream;


	@Inject
	AssessmentProcessor(
			FileReader fileReader,
			LinkParser linkParser,
			LinksValidator linksValidator,
			ParallelLinkFetcher parallelLinkFetcher,
			PrintStream outStream) {
		this.fileReader = requireNonNull(fileReader);
		this.linkParser = requireNonNull(linkParser);
		this.linksValidator = requireNonNull(linksValidator);
		this.parallelLinkFetcher = requireNonNull(parallelLinkFetcher);
		this.outStream = requireNonNull(outStream);
	}


	void process(Path file, List<String> base64EncodedMd5) {
		if (!exists(file)) {
			outStream.println("File not found");
			return;
		}

		// read file and parse for links - Part 1
		List<Link> links = linkParser.parse(fileReader.readLines(file));
		outStream.println(String.format("Links found: %s", links.size()));
		links.stream().map(this::format).forEach(outStream::println);
		// validate links - Part 2
		List<Link> validLinks = linksValidator.validate(links);
		outStream.println(String.format("%sValid links: %s", lineSeparator(), validLinks.size()));
		validLinks.stream().map(this::format).forEach(outStream::println);
		// fetch links - Part 3
		Map<String, FetchResult> fetchResults = parallelLinkFetcher.fetch(validLinks).stream()
				.collect(Collectors.toMap(FetchResult::getBase64, identity()));
		outStream.println(String.format("%sFetched links: %s", lineSeparator(), fetchResults.size()));
		fetchResults.values().stream().map(this::format).forEach(outStream::println);
		// find base64EncodedMd5 values - Part 4
		Map<String, Optional<FetchResult>> filteredFetchResults = base64EncodedMd5.stream()
				.collect(toMap(identity(), k -> ofNullable(fetchResults.get(k)))); // toMap() is not null-able
		outStream.println(
				String.format(
						"%s%sMatched Base64 Md5 Values: %s",
						lineSeparator(),
						lineSeparator(),
						filteredFetchResults.size()));
		filteredFetchResults.entrySet().stream().map(this::format).forEach(outStream::println);
	}


	private String format(Entry<String, Optional<FetchResult>> entry) {
		FetchResult result = entry.getValue().orElse(null);
		return result == null ? String.format("%s | not matched", entry.getKey())
				: String.format("%s  matches [%s]", entry.getKey(), result.getLink().getUrl());
	}


	private String format(FetchResult result) {
		return String.format(
				"%s  (md5:%s, base64:%s, fetched:%s)",
				result.getLink().getUrl(),
				result.getMd5(),
				result.getBase64(),
				result.isSuccessful());
	}


	private String format(Link link) {
		return link.getUrl();
	}
}
