package com.zeitwesen.assessment;

import static com.google.inject.Guice.createInjector;
import static com.zeitwesen.assessment.Util.getBase64EncodedMd5FromClasspath;
import static com.zeitwesen.assessment.Util.getPathFromClasspath;
import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;

import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import javax.inject.Inject;

import com.google.inject.AbstractModule;
import com.google.inject.Injector;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;


/*
 * cli parser from https://github.com/remkop/picocli
 */
@Command(name = "Assessment", mixinStandardHelpOptions = true)
public class App implements Runnable {

	private static final String DEFAULT_INPUT_FILE = "SPECIFICATION.txt";

	private final AssessmentProcessor assessmentProcessor;

	@Option(names = { "-f", "--file" }, arity = "0..1",
			description = "Absolute path to file to parse or empty to use default file.")
	private String cliFile;

	@Option(names = { "-b64", "--base64" }, arity = "0..*",
			description = "Base64 encoded MD5 values or empty to use default values.")
	private List<String> cliBase64EncodedMd5;

	private Path file;
	private List<String> base64EncodedMd5;


	@Inject
	App(AssessmentProcessor assessmentProcessor, PrintStream outStream) {
		this.assessmentProcessor = requireNonNull(assessmentProcessor);
	}


	@Override
	public void run() {
		file = cliFile == null ? getPathFromClasspath(DEFAULT_INPUT_FILE) : Paths.get(cliFile);
		base64EncodedMd5 = requireNonNull(ofNullable(cliBase64EncodedMd5).orElse(getBase64EncodedMd5FromClasspath()));
		assessmentProcessor.process(file, base64EncodedMd5);
	}


	/**
	 * just for testing
	 * 
	 * @return
	 */
	Path getFile() {
		return file;
	}


	/**
	 * just for testing
	 * 
	 * @return
	 */
	List<String> getBase64EncodedMd5() {
		return base64EncodedMd5;
	}


	/**
	 * Main method for startup
	 * @param args
	 */
	public static void main(String[] args) {
		// injection is probably a bit overkill :)
		Injector injector = createInjector(new GuiceModule());
		CommandLine.run(injector.getInstance(App.class), injector.getInstance(PrintStream.class), args);
	}

	private static class GuiceModule extends AbstractModule {
		@Override
		protected void configure() {
			bind(PrintStream.class).toProvider(PrintStreamProvider.class);
		}
	}
}
