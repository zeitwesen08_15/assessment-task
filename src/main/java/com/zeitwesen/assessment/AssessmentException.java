package com.zeitwesen.assessment;

/**
 * Generic {@link RuntimeException} to encapsulate any exceptions.
 * 
 * Hint: I would normally use more specific exceptions in larger projects ;-)
 */
class AssessmentException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	AssessmentException(String message) {
		super(message);
	}


	AssessmentException(Throwable cause) {
		super(cause);
	}

}
