package com.zeitwesen.assessment;

import static com.google.common.collect.ImmutableList.copyOf;
import static com.google.common.collect.ImmutableList.toImmutableList;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URISyntaxException;
import java.nio.file.FileSystemNotFoundException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;

import com.google.gson.JsonElement;
import com.google.gson.JsonParser;


class Util {

	private Util() {}

	private static final String DEFAULT_BASE64_MD5_VALUES_FILENAME = "/base64_encoded_md5.json";


	static List<String> getBase64EncodedMd5FromClasspath() {
		return copyOf(
				new JsonParser().parse(
						new InputStreamReader(Util.class.getResourceAsStream(DEFAULT_BASE64_MD5_VALUES_FILENAME)))
						.getAsJsonObject().get("base64_encoded_md5").getAsJsonArray().iterator()).stream()
								.map(JsonElement::getAsString).collect(toImmutableList());
	}


	static Path getPathFromClasspath(String fileName) {
		try {
			return Paths.get(requireZipFileSystem(Util.class.getClassLoader().getResource(fileName).toURI()));
		} catch (URISyntaxException e) {
			throw new AssessmentException(e);
		}
	}


	/**
	 * fixes problems when reading from fat jars
	 * see
	 * https://stackoverflow.com/questions/25032716/getting-filesystemnotfoundexception-from-zipfilesystemprovider-when-creating-a-p
	 */
	private static URI requireZipFileSystem(URI uri) {
		if ("jar".equals(uri.getScheme())) {
			try {
				FileSystems.getFileSystem(uri);
			} catch (FileSystemNotFoundException e) {
				try {
					FileSystems.newFileSystem(uri, Collections.emptyMap());
				} catch (IOException e1) {
					throw new AssessmentException(e);
				}
			}
		}
		return uri;
	}

}
