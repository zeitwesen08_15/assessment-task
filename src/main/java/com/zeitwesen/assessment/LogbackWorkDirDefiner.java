package com.zeitwesen.assessment;

import ch.qos.logback.core.PropertyDefinerBase;


/*
 * used in logback.xml to dynamically find current working directory - so it knows where to put the logfiles when
 * executed as fat-jar
 * 
 * inspired by https://stackoverflow.com/questions/25692963/logback-log-created-in-same-directory-as-far-at-build-time
 */
public class LogbackWorkDirDefiner extends PropertyDefinerBase {

	@Override
	public String getPropertyValue() {
		return getClass().getProtectionDomain().getCodeSource().getLocation().getFile();
	}
}
