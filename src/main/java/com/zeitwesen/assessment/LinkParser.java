package com.zeitwesen.assessment;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static java.util.Objects.requireNonNull;
import static java.util.regex.Pattern.compile;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.inject.Singleton;


/**
 * Parses given List of Strings for links
 */
@Singleton
class LinkParser {

	private static final Pattern LINK_PATTERN = compile(
			"(https?)://[a-z0-9+&@#//%?=~\\-_|!:,.;]*",
			Pattern.CASE_INSENSITIVE);


	/**
	 * 
	 * @param lines must not be null
	 * @return all links found in given lines as of part 1 in specification
	 */
	List<Link> parse(List<String> lines) {
		// can be safely processed in parallel as parseLinks() is thread safe, maybe a bit overkill :)
		return requireNonNull(lines).parallelStream().map(LinkParser::parseLinks).flatMap(List<String>::stream)
				.map(Link::of).distinct().collect(toImmutableList());
	}


	private static List<String> parseLinks(String line) {
		List<String> list = new ArrayList<>();
		Matcher urlMatcher = LINK_PATTERN.matcher(line);
		while (urlMatcher.find()) {
			list.add(line.substring(urlMatcher.start(0), urlMatcher.end(0)));
		}
		return list;
	}

}
