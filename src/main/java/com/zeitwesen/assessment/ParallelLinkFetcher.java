package com.zeitwesen.assessment;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static java.lang.Runtime.getRuntime;
import static java.util.Objects.requireNonNull;

import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;

import javax.inject.Inject;
import javax.inject.Singleton;


/*
 * Inspiration: https://www.baeldung.com/java-8-parallel-streams-custom-threadpool
 */
@Singleton
class ParallelLinkFetcher {

	private final LinkFetcher linkFetcher;

	@Inject
	ParallelLinkFetcher(LinkFetcher linkFetcher) {
		this.linkFetcher = requireNonNull(linkFetcher);
	}


	List<FetchResult> fetch(List<Link> links) {
		ForkJoinPool threadPool = new ForkJoinPool(getRuntime().availableProcessors());
		try {
			return threadPool.submit(
					() -> requireNonNull(links).parallelStream().map(linkFetcher::fetch).collect(toImmutableList()))
					.get();
		} catch (InterruptedException | ExecutionException e) {
			throw new AssessmentException(e);
		}
	}

}
