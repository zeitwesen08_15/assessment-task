package com.zeitwesen.assessment;

import static java.util.Objects.requireNonNull;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

import javax.inject.Singleton;

@Singleton
class FileReader {

	/**
	 * Reads all lines of file at given path
	 * 
	 * @param path must not be null
	 * @return List of all lines if successfull
	 * @throws AssessmentException in case of any error
	 */
	List<String> readLines(Path path) {
		try {
			return Files.readAllLines(requireNonNull(path));
		} catch (IOException e) {
			throw new AssessmentException(e);
		}
	}

}
