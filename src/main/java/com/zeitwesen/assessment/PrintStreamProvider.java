package com.zeitwesen.assessment;

import java.io.PrintStream;

import javax.inject.Provider;
import javax.inject.Singleton;

@Singleton
public class PrintStreamProvider implements Provider<PrintStream>{

	@Override
	public PrintStream get() {
		return System.out;
	}

}
