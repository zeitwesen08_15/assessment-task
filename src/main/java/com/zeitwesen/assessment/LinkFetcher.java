package com.zeitwesen.assessment;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static org.slf4j.LoggerFactory.getLogger;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;
import java.util.function.Supplier;

import javax.inject.Singleton;

import org.slf4j.Logger;

@Singleton
class LinkFetcher {

	private static final Logger LOG = getLogger(LinkFetcher.class);

	private static final int MAX_RETRIES = 1;
	private static final int FETCH_BUFFER_SIZE = 1_024 * 10;

	/**
	 * Fetches content of given link url and creates md5 hash Thread-safe!
	 * 
	 * @param link
	 * @return {@link FetchResult}
	 */
	FetchResult fetch(Link link) {
		try {
			String md5Str = hexToBinary(new Retrier<>(() -> fetchAndHash(link.getUrl()), MAX_RETRIES).retry());
			return FetchResult.sucess(md5Str, new String(Base64.getEncoder().encode(md5Str.getBytes())), link);
		} catch (AssessmentException e) {
			LOG.error("Fechting failed: {}", link.getUrl(), e);
			return FetchResult.fail(link);
		}
	}

	/**
	 * see
	 * https://stackoverflow.com/questions/3752981/convert-md5-array-to-string-java
	 * 
	 * @param digest
	 * @return
	 */
	private static String hexToBinary(byte[] digest) {
		StringBuilder hex = new StringBuilder();
		for (byte b : digest) {
			hex.append(format("%02x", b));
		}
		return hex.toString();
	}

	/**
	 * Inspirations:
	 * <ul>
	 * <li>https://docs.oracle.com/javase/tutorial/networking/urls/readingURL.html</li>
	 * <li>http://massapi.com/source/manual/ftpserver/core/src/main/java/org/apache/ftpserver/command/impl/MD5.java.html#177</li>
	 * <li>https://stackoverflow.com/questions/5470219/get-md5-string-from-message-digest</li>
	 * </ul>
	 */
	private static byte[] fetchAndHash(String url) {
		LOG.info("Fetching {}, Thread: {}", url, Thread.currentThread().getName());
		MessageDigest digest = getMd();
		try (InputStream is = new URL(url).openStream(); DigestInputStream dis = new DigestInputStream(is, digest);) {
			byte[] buffer = new byte[FETCH_BUFFER_SIZE];
			int read = dis.read(buffer);
			while (read > -1) {
				read = dis.read(buffer);
			}
			return dis.getMessageDigest().digest();
		} catch (IOException e) {
			throw new AssessmentException(e);
		}
	}

	private static MessageDigest getMd() {
		try {
			return MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new AssessmentException(e);
		}
	}

	/**
	 * Inspired by
	 * https://stackoverflow.com/questions/11692595/design-pattern-for-retrying-logic-that-failed
	 */
	private static class Retrier<T> {

		private final Supplier<T> function;
		private int retryCount;

		public Retrier(Supplier<T> function, int retryCount) {
			this.function = requireNonNull(function);
			this.retryCount = retryCount;
		}

		private T retry() {
			while (retryCount >= 0) {
				try {
					return function.get();
				} catch (AssessmentException e) {
					retryCount--;
					if (retryCount == -1) {
						throw e;
					}
					LOG.info("Fetching failed, retrying.", e);
				}
			}
			throw new IllegalStateException("Should never reach here");
		}
	}
}
