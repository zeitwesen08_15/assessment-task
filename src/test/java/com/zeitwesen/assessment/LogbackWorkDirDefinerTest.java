package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;


public class LogbackWorkDirDefinerTest {

	@Test
	public void test() {
		//bit of no-brainer test :)
		assertThat(
				new LogbackWorkDirDefiner().getPropertyValue(),
				is(LogbackWorkDirDefiner.class.getProtectionDomain().getCodeSource().getLocation().getFile()));
	}

}
