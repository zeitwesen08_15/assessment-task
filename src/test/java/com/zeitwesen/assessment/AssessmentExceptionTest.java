package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Test;

public class AssessmentExceptionTest {

	@Test
	public void testMsg() {
		String message = "msg";
		AssessmentException assessmentException = new AssessmentException(message);
		assertThat(assessmentException.getMessage(), is(message));
	}
	
	@Test
	public void testCause() {
		RuntimeException cause = new RuntimeException("to wrap");
		AssessmentException assessmentException = new AssessmentException(cause);
		assertThat(assessmentException.getCause(), is(cause));
	}

}
