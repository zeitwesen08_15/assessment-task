package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class FileReaderTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();
	
	@Test
	public void testReadLines() throws URISyntaxException {		
		List<String> lines = new FileReader().readLines(Paths.get(getClass().getClassLoader().getResource("SPECIFICATION.txt").toURI()));
		assertThat(lines.get(0), is("SPECIFICATION"));
		assertThat(lines, hasSize(33));		
	}
	
	@Test(expected=NullPointerException.class)
	public void testExceptionalNull() {		
		new FileReader().readLines(null);		
	}

	@Test
	public void testExceptional() {
		expectedException.expect(AssessmentException.class);
		new FileReader().readLines(Paths.get("not", "found", "file"));
	}
	
}
