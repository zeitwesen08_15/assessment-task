package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class LinkTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();


	@Test
	public void test() {
		String link = "link";
		Link checksum = Link.of(link);
		assertThat(checksum.getUrl(), is(link));		
		assertThat(checksum, is(Link.of(link)));
		assertThat(checksum.hashCode(), is(3321881));
		assertThat(checksum.toString(), is("Link [url=link]"));
	}


	@Test(expected = NullPointerException.class)
	public void testNull() {
		Link.of(null);
	}


	@Test
	public void testEmpty() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("URL of Link must not be empty!");
		Link.of("");
	}

}
