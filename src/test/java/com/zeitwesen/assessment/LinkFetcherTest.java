package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

import java.io.IOException;

import org.junit.Test;


public class LinkFetcherTest {

	@Test(timeout = 30000)
	public void testSuccess() throws IOException {
		/*
		 * REPRODUCE VIA TERMINAL
		 * > wget https://tools.ietf.org/rfc/rfc3986.txt
		 * > md5 rfc3986.txt
		 * MD5 (rfc3986.txt) = 45bc1160a280c34b923309ae91ff283a
		 * > echo -n '45bc1160a280c34b923309ae91ff283a' | openssl base64
		 * NDViYzExNjBhMjgwYzM0YjkyMzMwOWFlOTFmZjI4M2E=
		 */

		Link link = Link.of("https://tools.ietf.org/rfc/rfc3986.txt");
		assertThat(
				new LinkFetcher().fetch(link),
				is(
						FetchResult.sucess(
								"45bc1160a280c34b923309ae91ff283a",
								"NDViYzExNjBhMjgwYzM0YjkyMzMwOWFlOTFmZjI4M2E=",
								link)));
	}


	@Test(timeout = 30000)
	public void testFail() {
		Link link = Link.of("https://some.random.url");
		assertThat(new LinkFetcher().fetch(link), is(FetchResult.fail(link)));
	}

}
