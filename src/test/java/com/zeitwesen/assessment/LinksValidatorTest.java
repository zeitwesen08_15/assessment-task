package com.zeitwesen.assessment;

import static com.google.common.collect.ImmutableList.toImmutableList;
import static org.junit.Assert.assertThat;

import java.util.stream.Stream;

import org.hamcrest.Matchers;
import org.junit.Test;


public class LinksValidatorTest {

	@Test
	public void testSpec() {
		//tests against given spec (part 2)
		assertThat(
				new LinksValidator().validate(
						Stream.of(
								"https://joe:pwd@test.com/assessment-101/invalid.txt",
								"https://test.com/sub/fragment#invalid",
								"https://test.com/sub2/query?invalid=true",
								"https://test.com:443/sub-1",
								"https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force",
								"https://zeitwesendiylog.spreitzer.at/").map(Link::of).collect(toImmutableList())),
				Matchers.containsInAnyOrder(
						Link.of("https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force"),
						Link.of("https://zeitwesendiylog.spreitzer.at/")));
	}

}
