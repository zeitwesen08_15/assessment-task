package com.zeitwesen.assessment;

import org.hamcrest.Matchers;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;

import static com.google.common.collect.Lists.newArrayList;
import static org.junit.Assert.assertThat;
import static org.junit.rules.ExpectedException.none;
import static org.mockito.Mockito.*;


public class ParallelLinkFetcherTest {

    @Rule
    public ExpectedException expectedException = none();

    private static final Link LINK = Link.of("some_random_link");


    @Test
    public void test() {
        // prepare
        LinkFetcher linkFetcher = mock(LinkFetcher.class);
        when(linkFetcher.fetch(LINK)).thenReturn(FetchResult.fail(LINK));
        // test
        List<FetchResult> results = new ParallelLinkFetcher(linkFetcher).fetch(newArrayList(LINK, LINK, LINK));
        // assert
        assertThat(results, Matchers.everyItem(Matchers.is(FetchResult.fail(LINK))));
        verify(linkFetcher, times(3)).fetch(LINK);
    }


    @Test
    public void testExceptional() {
        LinkFetcher linkFetcher = mock(LinkFetcher.class);
        doThrow(new AssessmentException("some exception")).when(linkFetcher).fetch(LINK);
        expectedException.expect(AssessmentException.class);
        expectedException.expectMessage("some exception");
        new ParallelLinkFetcher(linkFetcher).fetch(newArrayList(LINK));
    }

}
