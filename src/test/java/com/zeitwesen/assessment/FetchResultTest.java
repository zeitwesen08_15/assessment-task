package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;


public class FetchResultTest {

	@Rule
	public ExpectedException expectedException = ExpectedException.none();


	@Test
	public void testSucessResult() {
		String md5 = "md5";
		String base64 = "base64";
		Link link = Link.of("link");
		FetchResult fetchResult = FetchResult.sucess(md5, base64, link);
		assertThat(fetchResult.isSuccessful(), is(true));
		assertThat(fetchResult.getMd5(), is(md5.toLowerCase()));
		assertThat(fetchResult.getBase64(), is(base64));
		assertThat(fetchResult.getLink(), is(link));
		assertThat(fetchResult.hashCode(), is(-188045574));
		assertThat(fetchResult.toString(), is("FetchResult [successful=true, md5=md5, base64=base64, link=Link [url=link]]"));
		assertThat(fetchResult, is(FetchResult.sucess(md5, base64, link)));
	}
	
	
	


	@Test
	public void testFailedResult() {
		Link link = Link.of("link");
		FetchResult fetchResult = FetchResult.fail(link);
		assertThat(fetchResult.isSuccessful(), is(false));
		assertThat(fetchResult.getMd5(), nullValue());
		assertThat(fetchResult.getBase64(), nullValue());
		assertThat(fetchResult.getLink(), is(link));
		assertThat(fetchResult.toString(), is("FetchResult [successful=false, md5=null, base64=null, link=Link [url=link]]"));
		assertThat(fetchResult, is(FetchResult.fail(link)));
	}


	@Test(expected = NullPointerException.class)
	public void testNull1() {
		FetchResult.sucess(null, null, null);
	}


	@Test(expected = NullPointerException.class)
	public void testNull2() {
		FetchResult.sucess("md5", null, null);
	}


	@Test(expected = NullPointerException.class)
	public void testNull3() {
		FetchResult.sucess("md5", "base64", null);
	}


	@Test
	public void testEmpty() {
		expectedException.expect(IllegalArgumentException.class);
		expectedException.expectMessage("Md5 of FetchResult must not be empty!");
		FetchResult.sucess("", "base64", null);
	}

}
