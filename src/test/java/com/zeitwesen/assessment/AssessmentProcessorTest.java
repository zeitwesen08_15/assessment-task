package com.zeitwesen.assessment;

import static com.google.common.collect.Iterables.getOnlyElement;
import static java.nio.file.Files.createTempFile;
import static java.util.Collections.singletonList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;


public class AssessmentProcessorTest {

	@Mock
	private FileReader fileReader;
	@Mock
	private LinkParser linkParser;
	@Mock
	private LinksValidator linksValidator;
	@Mock
	private ParallelLinkFetcher parallelLinkFetcher;
	@Mock
	private PrintStream outStream;

	private AssessmentProcessor assessmentProcessor;


	@Before
	public void before() {
		initMocks(this);
		assessmentProcessor = new AssessmentProcessor(
				fileReader,
				linkParser,
				linksValidator,
				parallelLinkFetcher,
				outStream);
	}


	@After
	public void after() {
		Mockito.reset(fileReader, linkParser, linksValidator, parallelLinkFetcher, outStream);
	}


	@Test
	public void testFileExists() throws IOException {
		// setup
		Path file = createTempFile("tmpfile", null);
		List<String> lines = singletonList("some text line");
		when(fileReader.readLines(file)).thenReturn(lines);
		List<Link> links = singletonList(Link.of("some_url"));
		when(linkParser.parse(lines)).thenReturn(links);
		when(linksValidator.validate(links)).thenReturn(links);
		when(parallelLinkFetcher.fetch(links)).thenReturn(singletonList(FetchResult.fail(getOnlyElement(links))));
		// test
		assessmentProcessor.process(file, singletonList("base64Md5Value"));
		// verify
		verify(outStream, Mockito.times(8)).println(anyString());
	}


	@Test
	public void testFileNotExists() throws IOException {
		Path file = Paths.get("not", "existing", "file");
		assessmentProcessor.process(file, singletonList("base64Md5Value"));
		verify(outStream).println("File not found");
		verifyZeroInteractions(fileReader, linkParser, linksValidator, parallelLinkFetcher, outStream);
	}

}
