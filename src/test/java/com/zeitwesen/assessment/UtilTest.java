package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

import org.junit.Test;


public class UtilTest {

	@Test
	public void testGetBase64EncodedMd5FromClasspath() {
		assertThat(
				Util.getBase64EncodedMd5FromClasspath(),
				contains("ZjIyMGVjZmNlN2YxOGU1MDJjMTE3ZDMwMDY1OTc3MjU="));
	}
	
	
	@Test
	public void test() {
		assertThat(Util.getPathFromClasspath("SPECIFICATION.txt"), notNullValue());
	}

}
