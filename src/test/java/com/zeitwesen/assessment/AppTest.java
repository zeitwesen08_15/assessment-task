package com.zeitwesen.assessment;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;

import java.io.PrintStream;
import java.net.URISyntaxException;
import java.nio.file.Paths;

import org.junit.Test;

import picocli.CommandLine;


public class AppTest {

	private static final AssessmentProcessor ASSESSMENT_PROCESSOR = mock(AssessmentProcessor.class);
	private static final PrintStream OUTSTREAM = mock(PrintStream.class);


	@Test
	public void testNoArguments() throws URISyntaxException {
		App app = new App(ASSESSMENT_PROCESSOR, OUTSTREAM);
		CommandLine.run(app, System.out, new String[] {});
		// README.md from cp
		assertThat(app.getFile(), is(Paths.get(getClass().getClassLoader().getResource("SPECIFICATION.txt").toURI())));
		// base64 values from cp
		assertThat(app.getBase64EncodedMd5(), contains("ZjIyMGVjZmNlN2YxOGU1MDJjMTE3ZDMwMDY1OTc3MjU="));
	}


	@Test
	public void testFileWithBase64() {
		App app = new App(ASSESSMENT_PROCESSOR, OUTSTREAM);
		CommandLine.run(
				app,
				System.out,
				"-f=someREADME.md -b64=base64_endcoded_md5_1 base64_endcoded_md5_2".split(" "));
		// someREADME.md from cli
		assertThat(app.getFile(), is(Paths.get("someREADME.md")));
		// base64 values from cli
		assertThat(app.getBase64EncodedMd5(), contains("base64_endcoded_md5_1", "base64_endcoded_md5_2"));
	}


	@Test
	public void testNoFileWithBase64() {
		App app = new App(ASSESSMENT_PROCESSOR, OUTSTREAM);
		CommandLine.run(app, System.out, "-b64=base64_endcoded_md5_1 base64_endcoded_md5_2".split(" "));
		// README.md from cp
		assertThat(app.getFile(), is(Util.getPathFromClasspath("SPECIFICATION.txt")));
		// base64 values from cp
		assertThat(app.getBase64EncodedMd5(), contains("base64_endcoded_md5_1", "base64_endcoded_md5_2"));
	}


	@Test
	public void testFileNoBase64() {
		App app = new App(ASSESSMENT_PROCESSOR, OUTSTREAM);
		;
		CommandLine.run(app, System.out, "-f customREADME.md".split(" "));
		// file from cli
		assertThat(app.getFile(), is(Paths.get("customREADME.md"))); 
		// base64 values from cp
		assertThat(app.getBase64EncodedMd5(), contains("ZjIyMGVjZmNlN2YxOGU1MDJjMTE3ZDMwMDY1OTc3MjU=")); 
	}
	
	
	@Test(timeout = 20000)
	public void testMain() {
		App.main(new String[] {});
	}
}
