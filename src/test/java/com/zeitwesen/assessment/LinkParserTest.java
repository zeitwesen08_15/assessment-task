package com.zeitwesen.assessment;

import static com.google.common.collect.Lists.newArrayList;
import static java.util.Collections.singletonList;
import static java.util.stream.Collectors.toList;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.stream.Stream;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;


public class LinkParserTest {

	@Test
	public void test() {
		assertThat(
				new LinkParser().parse(
						newArrayList(
								"first line with two hyperlinks http://test and http://test/ 1",
								"second line contains http://spreitzer.at and http://spreitzer.at/ 2",
								"third line has https://spreitzer.at and https://zeitwesendiylog.spreitzer.at/ 3",
								"fourth line has uppercase letters and numbers http://ORF.at and https://24now.co/f",
								"fith line has deeper links https://24now.co/u/zeitwesen or gmail: https://mail.google.com/mail/u/0/#inbox")),
				new LinkMatcher(
						"http://test",
						"http://test/",
						"http://spreitzer.at",
						"http://spreitzer.at/",
						"https://spreitzer.at",
						"https://zeitwesendiylog.spreitzer.at/",
						"http://ORF.at",
						"https://24now.co/f",
						"https://24now.co/u/zeitwesen",
						"https://mail.google.com/mail/u/0/#inbox"));
	}


	@Test
	public void testOnlyDistinctLinks() {
		assertThat(
				new LinkParser().parse(
						newArrayList(
								"has two same links http://test and http://test ")),
				new LinkMatcher(
						"http://test"));
	}
	
	@Test
	public void testSpec() {
		// regarding spec also "invalid" (as of part 2) links should be parsed and found
		assertThat(
				new LinkParser().parse(singletonList(
						"invalid links still to be parsed"
								+ "https://joe:pwd@test.com/assessment/invalid.txt "
										+ "https://test.com/sub/fragment#invalid " 
										+ "https://test.com/sub2/query?invalid=true "
										+ "https://test.com:443/sub-1 "
										+ "valid links "
										+ "https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force "
										+ "https://zeitwesendiylog.spreitzer.at/")),
				new LinkMatcher(
						"https://joe:pwd@test.com/assessment/invalid.txt",
						"https://test.com/sub/fragment#invalid",
						"https://test.com/sub2/query?invalid=true",
						"https://test.com:443/sub-1",
						"https://en.wikipedia.org/wiki/Internet_Engineering_Task_Force",
						"https://zeitwesendiylog.spreitzer.at/"));
	}


	@Test(expected = NullPointerException.class)
	public void testNull() {
		new LinkParser().parse(null);
	}

	/**
	 * Custom matcher to match List of {@link Link} elements
	 */
	private static class LinkMatcher extends TypeSafeMatcher<List<Link>> {

		private final List<String> linksToMatch;


		private LinkMatcher(String... strings) {
			linksToMatch = Stream.of(strings).sorted(String.CASE_INSENSITIVE_ORDER).collect(toList());
		}


		@Override
		public void describeTo(Description description) {}


		@Override
		protected boolean matchesSafely(List<Link> items) {
			return linksToMatch.equals(
					items.stream().map(Link::getUrl).sorted(String.CASE_INSENSITIVE_ORDER).collect(toList()));
		}

	}
}
