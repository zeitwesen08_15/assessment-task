# Assessment Task

## Specification
see [src/main/resources/SPECIFICATION.txt](../master/src/main/resources/SPECIFICATION.txt) for the full specification of this assessment task.



## Build and package
Build and package the artifact using `mvn clean package`.

## Run
After successfully building and packaging the artifact change into the `target` directory (`cd target`) where the fat-jar `assessment-task.jar` resides.

* `java -jar assessment-task.jar -h` show help
* `java -jar assessment-task.jar` parse default SPECIFICATION.txt and matches default base64 encoded md5 values (no args)
* `java -jar assessment-task.jar -f=/path/to/some/file.txt` parse given file and match default base64 encoded md5 values
* `java -jar assessment-task.jar -b64=base64_endcoded_md5_1 base64_endcoded_md5_2` parse default SPECIFICATION.txt and matche given base64 encoded md5
* `java -jar assessment-task.jar -f=/path/to/some/file.txt -b64=base64_endcoded_md5_1 base64_endcoded_md5_2` parses given file and matches given base64 encoded md5

The application logs error messages to <dir-of-jar>/assessment-task.jar-error.log

## Requirements
Java > 1.8, Maven

## Thoughts about my programming style
* **visibility** My strategy regarding class visibility is as low as possible, as high as necessary. e.g. API of libs need to be public, however "worker" classes to fulfill the requirements should not be public so they can not be "abused" by other components. And I can be free to refactor it anytime as I know it can not break other code relying on my class. That is why my whole app resides in only one package.

* **fail fast** I usually apply a "fail fast" strategy (e.g. `requireNonNull(object)`). It helps encourage strong API contracts.

* **null values** I normally do not return `null` in APIs. Instead I return e.g. an empty list or a typed object (see `FetchResult.fail(Link)`).

* **testability/tests** My code is written in a way that I can reach most parts in unit tests. My test cases usually are asserted unit-tests with an overall coverage > 90%.

* **immutable models** Writing immutable models has a lot of advantages in terms of thread-safety, serialization and of course while debugging as you know that values can only change when the model object is newly created and assigned.

* **thread-safety** Normally I try to write my classes thread-safe so they can be easily reused and/or parallelized.
* **static imports** I like static imports when it increases readability while still preserving the context. e.g. `requireNonNull(Object)` is a nice static import to me whereas `of(Object)` for `Stream.of(Object)` is not.
* **best practices** I'm usually following default java code format, recommendations of sonarlint/sonarqube, patterns and best practices as of books like "Effective Java".
